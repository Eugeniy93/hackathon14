from enum import Enum


class AgentActionType(Enum):
    SERVE = 0
    MOVE = 1
    FINISH = 2


class AgentAction:
    def __init__(self, type, car, target):
        self.type = type
        self.car = car
        self.target = target
