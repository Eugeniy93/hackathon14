import networkx as nx
import matplotlib.pyplot as plt
import math

from world import NodeType


class WorldView:
    def __init__(self, world):
        self.world = world
        self.pos = None
        self.invalidate = False
        self.closed = False

    def open(self):
        g = self.world.g
        self.pos = nx.spring_layout(g)

        plt.ion()
        plt.show()

        def on_key_press_event_handler(event):
            self.invalidate = True

        def on_close_event_handler(event):
            self.closed = True

        figure = plt.figure()
        figure.canvas.mpl_connect('key_press_event', on_key_press_event_handler)
        figure.canvas.mpl_connect('close_event', on_close_event_handler)

        self.update()

    def idle(self):
        while not self.invalidate and not self.closed:
            plt.pause(0.0001)

        if self.closed:
            return

        self.update()

    def update(self):
        plt.clf()

        g = self.world.g
        cars = self.world.cars
        pos = self.pos

        moving_cars = [c for c in cars if not c.is_free and c.transition is not None]
        free_cars = [c for c in cars if c.is_free]
        serving_cars = [c for c in cars if not c.is_free and c.position is not None]

        nx.draw(g, pos)

        node_labels = nx.get_node_attributes(g, 'data')
        nx.draw_networkx_labels(g, pos, labels=node_labels)

        banks = [n[0] for n in g.nodes(data=True) if n[1]['data'].type == NodeType.BANK]
        clients = [n[0] for n in g.nodes(data=True) if n[1]['data'].type == NodeType.CLIENT]
        empty_clients = [n[0] for n in g.nodes(data=True) if
                         n[1]['data'].type == NodeType.CLIENT and n[1]['data'].cash == 0]
        nx.draw_networkx_nodes(g, pos, nodelist=banks, node_color='r', node_size=1000)
        nx.draw_networkx_nodes(g, pos, nodelist=clients, node_color='g')
        nx.draw_networkx_nodes(g, pos, nodelist=empty_clients, node_color='#000000')

        free_cars_nodelist = [c.position for c in free_cars]
        serving_cars_nodelist = [c.position for c in serving_cars]
        nx.draw_networkx_nodes(g, pos, nodelist=free_cars_nodelist, node_color='#1f78b4', node_size=1000)
        nx.draw_networkx_nodes(g, pos, nodelist=serving_cars_nodelist, node_color='#FFFF00', node_size=1000)

        # custom_edge_labels = g.edges(data=True)
        data_edge_labels = nx.get_edge_attributes(g, 'data')
        nx.draw_networkx_edge_labels(g, pos, edge_labels=data_edge_labels)
        # nx.draw_networkx_edge_labels(g, pos)
        nx.draw_networkx_edges(g, pos, g.edges, edge_color='b', width=4)

        moving_cars_edgelist = [c.transition for c in moving_cars]
        nx.draw_networkx_edges(g, pos, moving_cars_edgelist, edge_color='r', width=8)

        plt.draw()
        plt.gcf().canvas.flush_events()
        self.invalidate = False


class WorldViewXY:
    def __init__(self, world):
        self.world = world
        self.pos = nx.get_node_attributes(self.world.g, 'pos')
        self.invalidate = False
        self.closed = False

    def open(self):
        g = self.world.g

        plt.ion()
        plt.show()

        def on_key_press_event_handler(event):
            self.invalidate = True

        def on_close_event_handler(event):
            self.closed = True

        figure = plt.figure()
        figure.canvas.mpl_connect('key_press_event', on_key_press_event_handler)
        figure.canvas.mpl_connect('close_event', on_close_event_handler)

        self.update()
        plt.gca().set_xlim((-100, 100))
        plt.gca().set_ylim((-100, 100))
        plt.gcf().set_size_inches(6, 6)

    def idle(self):
        while not self.invalidate and not self.closed:
            plt.pause(0.0001)

        if self.closed:
            return

        self.update()

    def update(self):
        plt.clf()

        g = self.world.g
        cars = self.world.cars
        pos = self.pos

        moving_cars = [c for c in cars if not c.is_free and c.transition is not None]
        free_cars = [c for c in cars if c.is_free]
        serving_cars = [c for c in cars if not c.is_free and c.position is not None]

        node_labels = nx.get_node_attributes(g, 'data')
        nx.draw_networkx_labels(g, pos, labels=node_labels)

        self.update_nodes(g)

        free_cars_nodelist = [c.position for c in free_cars]
        serving_cars_nodelist = [c.position for c in serving_cars]
        nx.draw_networkx_nodes(g, pos, nodelist=free_cars_nodelist, node_color='#1f78b4', node_size=400)
        nx.draw_networkx_nodes(g, pos, nodelist=serving_cars_nodelist, node_color='#FFFF00', node_size=400)

        # custom_edge_labels = g.edges(data=True)
        data_edge_labels = nx.get_edge_attributes(g, 'data')
        nx.draw_networkx_edge_labels(g, pos, edge_labels=data_edge_labels)
        # nx.draw_networkx_edge_labels(g, pos)
        nx.draw_networkx_edges(g, pos, g.edges, edge_color='b', width=4)

        moving_cars_edgelist = [c.transition for c in moving_cars]
        nx.draw_networkx_edges(g, pos, moving_cars_edgelist, edge_color='r', width=8)

        plt.draw()
        plt.gcf().canvas.flush_events()
        self.invalidate = False

    def update_nodes(self, g):
        banks = [n[0] for n in g.nodes(data=True) if n[1]['data'].type == NodeType.BANK]
        clients = [n[0] for n in g.nodes(data=True) if
                   n[1]['data'].type == NodeType.CLIENT and n[1]['data'].cash != 0]
        empty_clients = [n[0] for n in g.nodes(data=True) if
                         n[1]['data'].type == NodeType.CLIENT and n[1]['data'].cash == 0]
        nx.draw_networkx_nodes(g, self.pos, nodelist=banks, node_color='r', node_size=200)
        nx.draw_networkx_nodes(g, self.pos, nodelist=clients, node_color='g', node_size=200)
        nx.draw_networkx_nodes(g, self.pos, nodelist=empty_clients, node_color='#000000', node_size=200)