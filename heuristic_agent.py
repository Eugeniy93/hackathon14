from networkx import Graph
from world import NodeType
from agent import AgentActionType, AgentAction
import random


class HeuristicAgent:

    def permutate(self, world, solution):
        self.insert_remove(world, solution)
        return solution

    def equalize(self, world, solution):
        pass

    def insert_remove(self, world, solution):
        seen_points = set()
        for path in solution.paths.values():
            for loop in path.loops:
                seen_points.update(loop.points)

        all_points = set(world.g.nodes)
        unseen_points = all_points.difference(seen_points)
        unseen_points.remove(0)

        for path in solution.paths.values():
            for loop in path.loops:
                if random.randint(0, 100) <= 30:
                    loop.points[random.randint(0, len(loop.points) - 1)] = random.choice(tuple(unseen_points))

    def external_exchange(self, world, cars):
        pass

    def internal_exchange(self, worlds, cars):
        pass

    def disjoin(self, cars):
        pass
