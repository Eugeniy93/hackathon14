from agent import AgentAction, AgentActionType
from world import NodeType


class SuperGodAgentContext:
    def __init__(self, world, car_capacity):
        self.current_route = []
        self.max_reward = 0
        self.max_route = None
        self.world = world
        self.car_capacity = car_capacity

    def is_locked(self, target):
        return self.world.g.nodes[target]['locked']

    def lock(self, target):
        if target != 0:
            self.world.g.nodes[target]['locked'] = True

    def unlock(self, target):
        self.world.g.nodes[target]['locked'] = False

    def route_push(self, target):
        self.current_route.append(target)

    def route_pop(self, target):
        self.current_route.pop()

    def route_save(self, reward):
        if reward > self.max_reward:
            print("{} > {}".format(reward, self.max_reward))
            self.max_reward = reward
            self.max_route = self.current_route.copy()


class SuperGodAgent:
    def __init__(self):
        pass

    def calc_rewards(self, context, score, world_time, car_cash, start):
        context.route_push(start)

        result = [(target, self.calc_reward_wrapper(context, score, world_time, car_cash, start, target))
                  for target in context.world.g.neighbors(start)
                  if not context.is_locked(target)]

        context.route_pop(start)
        return result

    def calc_reward_wrapper(self, context, score, world_time, car_cash, start, target):
        context.lock(target)
        reward = self.calc_reward(context, score, world_time, car_cash, start, target)
        context.unlock(target)
        return reward

    def calc_reward(self, context, score, world_time, car_cash, start, target):
        world = context.world
        target_data = world.g.nodes[target]['data']

        edge = world.g.edges[(start, target)]
        traffic = world.traffic_generator.get_traffic(edge, world.normal_time_for(world_time))
        feature_time = world_time + traffic * edge['data'].length + target_data.time
        # обработка ноды должна укдадываться в рабочее время.
        # и для возврата в банк и для обслуживания клиента.
        # поэтому кумулятивная награда за обработку ноды с превышением времени равна нулю - тупиковая ветвь
        if feature_time > world.end_time:
            return 0

        # цель - обслужить клиента
        if target_data.type == NodeType.CLIENT:
            # запрещаем посещать уже обслуженных клиентов. это допущение этого алгоритма
            # поэтому кумулятивная награда равна нулю - тупиковая ветвь
            if target_data.cash == 0:
                return 0

            feature_cash = car_cash + target_data.cash

            # при обслуживании клиента превышать лимит денег запрещено
            # поэтому кумулятивная награда за клиента с превышением лимита равна нулю - тупиковая ветвь
            if feature_cash > context.car_capacity:
                return 0

            # иначе кумулятивная награда равняется самому выгодному дальнейшему действию
            rewards = self.calc_rewards(context, score, feature_time, feature_cash, target)
            best_target, best_reward = max(rewards, key=lambda i: i[1])
            return best_reward

        # цель - возвращение в банк
        if target_data.type == NodeType.BANK:
            # print("route: {}".format(context.current_route))
            feature_score = score + car_cash
            rewards = self.calc_rewards(context, feature_score, feature_time, 0, target)
            if not rewards:
                # кончились точки для обхода
                reward = feature_score
            else:
                best_target, best_reward = max(rewards, key=lambda i: i[1])
                reward = best_reward
                if best_reward == 0:
                    # print("route: {} - {}".format(feature_score, context.current_route))
                    reward = feature_score
            context.route_save(reward)
            return reward

    def get_action(self, car, world):
        start_node_data = world.g.nodes[car.position]['data']

        # в целях упрощения предполагается что если машина отправилась к клиену то она его и обслуживает
        if start_node_data.type == NodeType.CLIENT and start_node_data.is_free and start_node_data.cash > 0:
            return AgentAction(type=AgentActionType.SERVE, car=car, target=car.position)

        # в целях упрощения предполагается что если машина в банке и у неё есть деньги то она разгружается
        if start_node_data.type == NodeType.BANK and car.cash > 0:
            return AgentAction(type=AgentActionType.SERVE, car=car, target=car.position)

        context = SuperGodAgentContext(world, car.capacity)
        rewards = self.calc_rewards(context, 0,
                                    world.current_time, car.cash,
                                    car.position)
        best_move, best_reward = max(rewards, key=lambda i: i[1])
        print("best_reward: {}".format(best_reward))
        print("max_reward: {}".format(context.max_reward))
        print("max_reward_route: {}".format(context.max_route))

        if best_reward > 0:
            return AgentAction(type=AgentActionType.MOVE, car=car, target=best_move)
        else:
            return AgentAction(type=AgentActionType.FINISH, car=car, target=car.position)
