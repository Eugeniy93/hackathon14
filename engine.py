import websocket, threading
import json
from world import Car, World, NodeData, NodeType, EdgeData
from networkx import Graph
import time
import re
from threading import Lock


class Engine:
    websocket_url = "ws://172.30.13.151:8080/race"
    # websocket_url = "ws://172.30.9.50:8080/race"

    def __init__(self, lock):
        self.ws = websocket.WebSocketApp(Engine.websocket_url, on_message=self.on_message, on_error=self.on_error,
                                         on_close=self.on_close, on_open=self.on_open)
        self.world = World()
        self.token = None
        self.routes = None
        self.traffic = None
        self.points = None
        self.teamsum = None
        self.lock = lock

    def moveCar(self, car, node):
        data = {"goto": node, "car": car.car_name}
        # Есть возможность посылать, что денег не собираем data = {"goto": node, "car": car.car_name, nomoney: true}
        print("Передвигаем тачку \n{}".format(data))
        print("У тачки деньги {} и время {} в ноду {} куда она едет, денег: {}".format(car.cash, car.car_time, node,
                                                                                       self.world.g.nodes[node][
                                                                                           'data'].cash))
        car.wait_server_response = True
        self.world.waiting_traffic_refresh = True
        self.ws.send(json.dumps(data))

    def on_message(self, message):
        try:
            Lock.acquire(self.lock)
            # Защита от того, что сервер шлет гавно
            message = re.sub(r'}\n{', ',', message)
            try:
                print("Пришли данные с сервера {}".format(message))
                msg = json.loads(message)
                # Псевдотокен для работы. Нужен только в случае реконнекта при потере сввязи.
                if 'token' in msg:
                    self.fillTokenAndCars(msg)
                # Маршруты движения
                if 'routes' in msg:
                    self.routes = msg['routes']
                if 'traffic' in msg:
                    if self.world.g is None:
                        self.traffic = msg['traffic']
                    else:
                        self.updateTraffic(msg['car'], msg['traffic'])
                if 'points' in msg:
                    self.points = msg['points']
                if 'point' in msg:
                    self.updateCarPosition(msg['car'], msg['point'], msg['carsum'])
                if 'teamsum' in msg:
                    self.teamsum = msg['teamsum']
                    print("На данный момент собрано {} денег".format(self.teamsum))
                # спорная штука, зависит от того, как приходят маршруты и точки, могут ли они перепосылаться сервером.
                if self.world.g is None and self.routes is not None and self.traffic is not None and self.points is not None:
                    self.buildWorldGraph()
            except:
                print("Возникла ошибка при примеме данных!")
                raise BaseException
        finally:
            Lock.release(self.lock)

    def updateCarPosition(self, car_name, position, carsum):
        for car in self.world.cars:
            if car.car_name == car_name:
                edge_data = Graph.get_edge_data(self.world.g, car.position, position)['data']
                car.car_time += edge_data.traffic[car_name] * edge_data.length
                car.position = position
                car.wait_server_response = False
                # car.cash = carsum

    def fillTokenAndCars(self, message):
        self.token = message['token']
        cars = message['cars']  # Обратите внимание на убогий ключ
        for car_name in cars:
            car = Car(1000000, 0, car_name)
            self.world.add_car(car)

    def on_error(self, error):
        # send reconnect
        print(error)

    def on_close(self):
        print("### websocket closed ###")
        print("### try to reconnect ###")
        websocket.send(json.dumps({"reconnect": self.token}))

    def on_open(self):
        print("### opened websocket ###")

    def register_team(self):
        print("Регистрируем себя в системе")
        self.ws.send(json.dumps({"team": "digitalc"}))

    def updateTraffic(self, car, traffics):
        for traffic in traffics:
            edge = Graph.get_edge_data(self.world.g, traffic['a'], traffic['b'])
            edge['data'].traffic[car] = float(traffic['jam'])
            # Непонятно используется ли вообще
        self.world.waiting_traffic_refresh = False

    def buildWorldGraph(self):
        self.world.g = Graph()
        try:
            for node in self.points:
                node_type = NodeType.CLIENT
                if node['p'] == 1:
                    node_type = NodeType.BANK
                Graph.add_node(self.world.g, node['p'],
                               data=NodeData(id=node['p'], type=node_type, time=0, cash=node['money']),
                               locked=False)
            for edge in self.routes:
                a = edge['a']
                b = edge['b']
                Graph.add_edge(self.world.g, a, b, data=EdgeData(id=(a, b), traffic=0.0, length=edge['time']))
            for traffic in self.traffic:
                edge = Graph.get_edge_data(self.world.g, traffic['a'], traffic['b'])

                t = float(traffic['jam'])
                edge['data'].traffic = {"sb0": t, "sb1": t, "sb2": t, "sb3": t, "sb4": t}
                # Непонятно используется ли вообще
            print("Построен граф")
        except:
            print("Ошибка при построении графа")
            self.world.g = None
            raise BaseException
