import os
import time

from agent import AgentActionType
from god_agent import GodAgent
from agent88branch import Agent88branch
from generator import MapGeneratorXY, MapGenerator, TrafficGenerator
from view import WorldViewXY, WorldView
from world import World, Car


class Starter:
    def __init__(self):
        pass

    def start(self):
        filename = "results.csv"
        if os.path.isfile(filename):
            os.remove(filename)
        write(filename, "ui\txy_mode\tseed\tnode_count\tcar_count\tagent\tcar_capacity\tworld.get_score()\t"
                        "len(world.get_fail_cars()\tworld.is_time_over()\tworld.is_cash_over()\tcurrent_milli_time - "
                        "start)")

        ui = [False]
        xy_mode = [False, True]
        seed = [30, 300, 3000, 30000, 50, 500, 5000, 50000]
        node_count = [250, 500]
        car_count = [1, 3, 5]
        agents = ["GodAgent", "Agent88branch"]
        car_capacity = [1000000]
        for u in ui:
            for xy in xy_mode:
                for s in seed:
                    for nc in node_count:
                        for cc in car_count:
                            for a in agents:
                                for cap in car_capacity:
                                    try:
                                        agent = None
                                        if a == "GodAgent":
                                            agent = GodAgent()
                                        if a == "Agent88branch":
                                            agent = Agent88branch(branches=2, time_limit=0.5, edge_limit=3);
                                        it = self.do_it(u, xy, s, nc, cc, agent, cap)
                                        write(filename, "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t"
                                              .format(it[0], it[1], it[2], it[3], it[4], it[5], it[6], it[7], it[8],
                                                      it[9], it[10], it[11]))
                                    except Exception:
                                        write(filename, "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t"
                                              .format(u, xy, s, nc, cc, agent, cap, "error!"))

    def do_it(self, ui, xy_mode, seed, node_count, car_count, agent, car_capacity):
        if xy_mode:
            map_generator = MapGeneratorXY()
        else:
            map_generator = MapGenerator()
        traffic_generator = TrafficGenerator()

        world = World()
        world.g = map_generator.gen(node_count, seed)
        world.traffic_generator = traffic_generator
        for i in range(0, car_count):
            world.add_car(Car(capacity=car_capacity, node=0))

        if ui:
            if xy_mode:
                view = WorldViewXY(world)
            else:
                view = WorldView(world)
            view.open()

        start = current_milli_time()

        while not world.is_time_over() and not world.is_cash_over():
            world.update_traffic()

            for car in world.get_free_cars():
                agent_action = agent.get_action(car, world)
                if agent_action.type == AgentActionType.MOVE:
                    world.move(agent_action.car, agent_action.target)
                elif agent_action.type == AgentActionType.SERVE:
                    world.serve(agent_action.car, agent_action.target)
                elif agent_action.type == AgentActionType.FINISH:
                    pass
                else:
                    raise ValueError("Incorrect agent action type: {}".format(agent_action.type))

            if ui:
                view.idle()

            world.skip()
            world.act()
            print("-----------------------------------------------")

        print("-----------------------------------------------")
        print("-----------------------------------------------")
        print("-----------------------------------------------")
        print("score: {}".format(world.get_score()))
        print("fail car: {}".format(len(world.get_fail_cars())))
        print("is_time_over: {}".format(world.is_time_over()))
        print("is_cash_over: {}".format(world.is_cash_over()))
        print("time: {}".format(current_milli_time() - start))

        return ui, xy_mode, seed, node_count, car_count, agent, car_capacity, world.get_score(), len(
            world.get_fail_cars()), world.is_time_over(), world.is_cash_over(), current_milli_time() - start


def current_milli_time():
    return int(round(time.time() * 1000))


def write(filename, value):
    with open(filename, 'a') as json_file:
        json_file.write(value + "\n")
        # json.dump(value, json_file)
