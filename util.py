from world import World, Car, Empty, NodeType
from config import XY_MODE
from generator import MapGenerator, TrafficGenerator, MapGeneratorXY


def print_result(world, solution):
    print("-----------------------------------------------")
    print("score: {}".format(world.get_score()))
    print(solution)
    print("-----------------------------------------------")


def generate():
    return generate_internal(5, 250, 1000000)


def generate_internal(cars_number, nodes, capacity):
    if XY_MODE:
        map_generator = MapGeneratorXY()
    else:
        map_generator = MapGenerator()
    traffic_generator = TrafficGenerator()

    world = World()
    world.g = map_generator.gen(nodes)
    world.traffic_generator = traffic_generator
    for i in range(1, cars_number + 1):
        world.add_car(Car(capacity=capacity, node=0))

    for node in world.g.nodes:
        world.g.nodes[node]['data'].time = 0

    return world


def emulate(world, solution):
    cursor = {}
    while not world.is_time_over() and not world.is_cash_over():
        world.update_traffic()
        for car in world.get_free_cars():
            node_data = world.get_node(car.position).data
            if (node_data.type == NodeType.CLIENT and node_data.cash > 0) or (node_data.type == NodeType.BANK and car.cash > 0):
                world.serve(car, car.position)
                continue

            car_cursor = cursor.get(car)
            if car_cursor is None:
                car_cursor = cursor[car] = 0

            point = solution.get_point(car.number, car_cursor)
            if point is None:
                if car.position == 0:
                    continue
                world.move(car, 0)
            else:
                world.move(car, point)
            cursor[car] += 1

        world.skip()
        world.act()
