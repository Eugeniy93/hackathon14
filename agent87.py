from world import NodeType
from agent import AgentAction, AgentActionType

SAFE = 1.1


def score_function(cash, time):
    return cash ** 2 / time


class Path88:
    def __init__(self, path=None, cash=None, time=None):
        self.path = path
        self.cash = cash
        self.time = time

    def score(self):
        return score_function(self.cash, self.time)


class Agent87:
    def __init__(self, branches=3):
        self.cache = {}
        self.busy_nodes = []
        self.free_nodes = {}
        self.branches = branches

    def get_action(self, car, world):
        start_node_data = world.g.nodes[car.position]['data']
        # в целях упрощения предполагается что если машина отправилась к клиену то она его и обслуживает
        if start_node_data.type == NodeType.CLIENT and start_node_data.is_free and start_node_data.cash > 0:
            return AgentAction(type=AgentActionType.SERVE, car=car, target=car.position)
        # в целях упрощения предполагается что если машина в банке и у неё есть деньги то она разгружается
        if start_node_data.type == NodeType.BANK and car.cash > 0:
            return AgentAction(type=AgentActionType.SERVE, car=car, target=car.position)

        self.update_busy_points(car, world)

        car.actions = self.get_start(car, world)
        if len(car.actions.path) == 1:
            return AgentAction(AgentActionType.FINISH, car, 0)

        while car.actions.cash < car.capacity:
            if not self.try_add_point(car, world, car.actions):
                return AgentAction(AgentActionType.MOVE, car, car.actions.path[1])

    def get_start(self, car, world):
        start_node_data = world.g.nodes[car.position]['data']
        path88 = Path88(time=world.g.nodes[0]['data'].time, cash=car.cash)

        if start_node_data.type != NodeType.BANK:
            path88.time += CalcTransitResult.get_edge_time(world, car.position, 0)
            path88.path = [car.position, 0]
            return path88

        if start_node_data.type == NodeType.BANK:
            better_node = None
            better_transit = None
            for free_node in self.free_nodes:
                calc_transit = CalcTransitResult(world, car, car.position, 0, free_node, path88)
                if calc_transit.is_available(world, car):
                    if calc_transit.is_better_than(better_transit):
                        better_node = free_node
                        better_transit = calc_transit
                    print(calc_transit)

            if better_node is None:
                return Path88(path=[car.position], time=0, cash=0)
            else:
                path88.path = [car.position, better_node]
                print("the best is {}, path: {}".format(better_transit, path88.path))
                path88.time += better_transit.transit_serv_time + better_transit.move_time
                path88.cash += better_transit.transit_cash
                self.busy_nodes.append(better_node)
                del self.free_nodes[better_node]
                return path88

    def try_add_point(self, car, world, path88):
        better_node = None
        better_position = None
        better_transit = None
        for i in range(1, len(path88.path)):
            for free_node in self.free_nodes:
                calc_transit = CalcTransitResult(world, car, path88.path[i - 1], path88.path[i], free_node, path88)
                if calc_transit.is_available(world, car):
                    if calc_transit.is_better_than(better_transit):
                        better_node = free_node
                        better_transit = calc_transit
                        better_position = i
                    print(calc_transit)

        if better_node is None:
            return False
        else:
            path88.path.insert(better_position, better_node)
            print("the best is {}, path: {}".format(better_transit, path88.path))
            path88.time += better_transit.transit_serv_time + better_transit.move_time
            path88.cash += better_transit.transit_cash
            self.busy_nodes.append(better_node)
            del self.free_nodes[better_node]
            return True

    def update_busy_points(self, current_car, world):
        # чистим свое и занятые тк будем перестраивать маршрут
        current_car.action_list = []
        self.busy_nodes = []
        # добавляем все чужие
        if len(world.cars) > 1:
            for c in world.cars:
                if c.actions is not None:
                    for a in c.actions.path:
                        self.busy_nodes.append(a)
        # добавляем всех незанятых клиентов с баблом
        self.free_nodes = {}
        for n in world.g.nodes:
            data = world.g.nodes.get(n)['data']
            if data.type == NodeType.CLIENT and n not in self.busy_nodes and data.cash > 0:
                self.free_nodes[n] = world.g.nodes.get(n)['data']


class CalcTransitResult:
    def __init__(self, world, car, from_node, to_note, transit_node, path88):
        self.path88 = path88
        self.from_node = from_node
        self.to_note = to_note
        self.transit_node = transit_node
        if from_node == to_note:
            self.direct_time = 0
        else:
            self.direct_time = self.get_edge_time(world, from_node, to_note)
        self.from_transit_time = self.get_edge_time(world, from_node, transit_node)
        self.transit_to_time = self.get_edge_time(world, transit_node, to_note)
        self.move_time = self.from_transit_time + self.transit_to_time - self.direct_time
        self.transit_cash = world.g.nodes[transit_node]['data'].cash
        self.transit_serv_time = world.g.nodes[transit_node]['data'].time
        if self.move_time + self.transit_serv_time != 0:
            self.transit_score = score_function(self.transit_cash, self.move_time + self.transit_serv_time)
        else:
            self.transit_score = score_function(self.transit_cash, 0.25)

        self.path_end_time = (path88.time + self.move_time) * SAFE + self.transit_serv_time + world.current_time
        self.path_end_cash = path88.cash + self.transit_cash
        self.path_end_score = score_function(self.path_end_cash, path88.time + self.move_time + self.transit_serv_time)

    def is_available(self, world, car):
        return self.path_end_time < world.end_time and self.path_end_cash < car.capacity

    def is_better_than(self, other_transit):
        return other_transit is None or other_transit.transit_score < self.transit_score
        # return other_transit is None or (other_transit.transit_score < self.transit_score and self.path_end_score > self.path88.score())

    @staticmethod
    def get_edge_time(world, from_node, to_note, time=None):
        return world.g.edges[(from_node, to_note)]['data'].get_width(world, time)

    def __str__(self):
        return "to {}: {}s + {}s, ${} + ${}, score: {} transit_score: {} -> {} ".format(self.transit_node,
                                                                                       self.transit_serv_time +
                                                                                       round(self.move_time), round(self.path88.time),
                                                                                       self.transit_cash,
                                                                                       self.path88.cash,
                                                                                       round(self.transit_score),
                                                                                       round(self.path88.score()),
                                                                                       round(self.path_end_score))

