import networkx as nx
import time

from enum import Enum

from agent import AgentAction, AgentActionType
from world import NodeType


class GodAgent:
    def __init__(self):
        self.depth_limit_enabled = True
        self.depth_limit = 5  # TODO должен вычисляться исходя из средне-оптимальной длинны пути

        self.cache_enabled = False
        self.cache = {}

        self.real_time_limit_enabled = True
        self.real_time_limit = 0.5

        self.concurrent_enabled = False
        self.routes = {}

    def __str__(self):
        return "GodAgent"

    @staticmethod
    def is_locked(world, target):
        return world.g.nodes[target]['locked']

    @staticmethod
    def lock(world, target):
        target_data = world.g.nodes[target]['data']
        if target_data.type != NodeType.BANK:
            world.g.nodes[target]['locked'] = True

    @staticmethod
    def unlock(world, target):
        world.g.nodes[target]['locked'] = False

    def calc_reward_lock_wrapper(self, context, depth, current_time, current_cash, start, target):
        context.lock(target)
        reward = self.calc_reward(context, depth, current_time, current_cash, start, target)
        context.unlock(target)
        return reward

    def calc_rewards(self, context, depth, current_time, current_cash, start):
        if self.cache_enabled and (context.car_capacity, current_time, current_cash, start) in self.cache:
            return self.cache[(context.car_capacity, current_time, current_cash, start)]

        context.route_push(start)

        targets = [target for target in context.world.g.neighbors(start) if not self.is_locked(context.world, target)]
        targets.sort(reverse=True,
                     key=lambda target: self.move_reward_function(context.world, current_time, start, target))
        result = [(target, self.calc_reward_lock_wrapper(context, depth,
                                                         current_time, current_cash,
                                                         start, target))
                  for target in targets]

        context.route_pop(start)

        if self.cache_enabled:
            self.cache[(context.car_capacity, current_time, current_cash, start)] = result

        return result

    # эвристическая функция оценки хода
    @staticmethod
    def move_reward_function(world, time, start, target):
        target_data = world.g.nodes[target]['data']
        edge = world.g.edges[(start, target)]
        # traffic = world.traffic_generator.get_traffic(edge, world.normal_time_for(time))
        traffic = edge['data'].traffic
        if edge['data'].length == 0 or traffic == 0:
            # TODO в рамках данной эвристики необходимо брать последнее ненулевое расстояние
            return target_data.cash ** 4
        return target_data.cash / (edge['data'].length * traffic + target_data.time)**2

    def calc_reward(self, context, depth, current_time, current_cash, start, target):
        world = context.world
        target_data = world.g.nodes[target]['data']

        edge = world.g.edges[(start, target)]
        # feature_time = current_time + edge['data'].get_width(world, current_time) + target_data.time
        feature_time = current_time + edge['data'].traffic * edge['data'].length + target_data.time
        # обработка ноды должна укдадываться в рабочее время.
        # и для возврата в банк и для обслуживания клиента.
        # поэтому кумулятивная награда за обработку ноды с превышением времени равна нулю - тупиковая ветвь
        if feature_time > world.end_time:
            return 0

        if self.real_time_limit_enabled and time.time() - context.real_time_start > self.real_time_limit:
            return 0

        # цель - обслужить клиента
        if target_data.type == NodeType.CLIENT:
            # запрещаем посещать уже обслуженных клиентов. это допущение этого алгоритма
            # поэтому кумулятивная награда равна нулю - тупиковая ветвь
            if target_data.cash == 0:
                return 0

            cumulative_cash = current_cash + target_data.cash

            # при обслуживании клиента превышать лимит денег запрещено
            # поэтому кумулятивная награда за клиента с превышением лимита равна нулю - тупиковая ветвь
            if cumulative_cash > context.car_capacity:
                return 0

            # ограничиваем глубину поиска
            # поэтому кумулятивная награда за превышение глубины равна нулю - тупиковая ветвь
            if self.depth_limit_enabled and depth >= self.depth_limit:
                return 0

            # иначе кумулятивная награда равняется самому выгодному дальнейшему действию
            rewards = self.calc_rewards(context, depth + 1,
                                        feature_time, cumulative_cash,
                                        target)
            best_target, best_reward = max(rewards, key=lambda i: i[1])
            return best_reward

        # цель - возвращение в банк
        if target_data.type == NodeType.BANK:
            # если машина вернулась в банк вовремя то за награду берём
            # количество денег которое машина собрала на текущем маршруте
            reward = self.cycle_reward_function(context, current_cash, feature_time)
            if reward > context.max_reward:
                context.update_max_reward_route(reward)
            return reward

    # эвристическая функция оценки ценности найденного цикла
    @staticmethod
    def cycle_reward_function(context, car_cash, world_time):
        return car_cash ** 4 / (world_time - context.world_time_start)

    def get_action(self, car, world):
        # start_node_data = world.g.nodes[car.position]['data']
        #
        # в целях упрощения предполагается что если машина отправилась к клиену то она его и обслуживает
        # if start_node_data.type == NodeType.CLIENT and start_node_data.is_free and start_node_data.cash > 0:
        #     return AgentAction(type=AgentActionType.SERVE, car=car, target=car.position)
        #
        # в целях упрощения предполагается что если машина в банке и у неё есть деньги то она разгружается
        # if start_node_data.type == NodeType.BANK and car.cash > 0:
        #     return AgentAction(type=AgentActionType.SERVE, car=car, target=car.position)

        if self.concurrent_enabled and car in self.routes:
            world.unlock_all(self.routes[car])

        context = CalcContext(world, car.capacity, world.current_time, time.time())
        rewards = self.calc_rewards(context, 0, world.current_time, car.cash, car.position)
        best_move, best_reward = max(rewards, key=lambda i: i[1])
        print("best reward: {}".format(best_reward))
        print("best route: {}".format(context.max_route))

        if self.concurrent_enabled and context.max_route:
            world.lock_all(context.max_route)
            self.routes[car] = context.max_route.copy()

        if best_reward > 0:
            world.lock(best_move)
            return AgentAction(type=AgentActionType.MOVE, car=car, target=best_move)
        else:
            return AgentAction(type=AgentActionType.FINISH, car=car, target=car.position)


class CalcContext:
    def __init__(self, world, car_capacity, world_time_start, real_time_start):
        self.current_route = []
        self.max_reward = 0
        self.max_route = None
        self.world = world
        self.car_capacity = car_capacity
        self.world_time_start = world_time_start
        self.real_time_start = real_time_start

    def is_locked(self, target):
        return self.world.g.nodes[target]['locked']

    def lock(self, target):
        target_data = self.world.g.nodes[target]['data']
        if target_data.type != NodeType.BANK:
            self.world.g.nodes[target]['locked'] = True

    def unlock(self, target):
        self.world.g.nodes[target]['locked'] = False

    def route_push(self, target):
        self.current_route.append(target)

    def route_pop(self, target):
        self.current_route.pop()

    def route_save(self, reward):
        if reward > self.max_reward:
            print("{} > {}".format(reward, self.max_reward))
            self.max_reward = reward
            self.max_route = self.current_route.copy()

    def node_data(self, target):
        return self.world.g.nodes[target]['data']

    def node(self, target):
        return self.world.g.nodes[target]

    def edge_data(self, start, target):
        return self.edge(start, target)['data']

    def edge(self, start, target):
        return self.world.g.edges[(start, target)]

    def unlocked_neighbors(self, start):
        return [target for target in self.world.g.neighbors(start) if not self.is_locked(target)]

    def update_max_reward_route(self, new_max_reward):
        # print("{} > {}".format(new_max_reward, self.max_reward))
        self.max_reward = new_max_reward
        self.max_route = self.current_route.copy()
