from threading import Lock

from networkx import Graph
from world import NodeType
from agent import AgentActionType, AgentAction


class GreedyAgent:

    def __init__(self, lock):
        self.lock = lock

    def get_action(self, car, world):
        # serve = self.try_serve(car, world)
        # if serve is not None:
        #     return serve
        try:
            Lock.acquire(self.lock)
            greedy_path = self.get_greedy_path(car, world)
            sumInleftedNodes = 0
            for node in greedy_path[1:]:
                if node != 1:
                    sumInleftedNodes += world.g.nodes[node]['data'].cash
            sumInWorld = 0
            for node in world.g.nodes:
                if node != 1:
                    sumInWorld += world.g.nodes[node]['data'].cash
            print("Сумма денег в оставшихся нодах, куда мы доберемся: {}".format(sumInleftedNodes))
            print("Сумма денег в мире: {}".format(sumInWorld))
            print("Жадный путь {}".format(greedy_path))
            # В оставшихся нодах мало денег, идем в банк.
            if sumInleftedNodes < 150_000 and sumInleftedNodes < 0.7 * sumInWorld and car.position != 1 and sumInWorld > (
                    car.capacity - car.cash):
                return AgentAction(type=AgentActionType.MOVE, car=car, target=1)
            if len(greedy_path) == 0:
                return AgentAction(type=AgentActionType.FINISH, car=car, target=car.position)
            elif len(greedy_path) == 1 and greedy_path[0] == 1:
                return AgentAction(type=AgentActionType.MOVE, car=car, target=1)
            else:
                return AgentAction(type=AgentActionType.MOVE, car=car, target=greedy_path[0])
        finally:
            Lock.release(self.lock)

    def get_greedy_path(self, car, world):
        bank_nodes = [n for n in world.g.nodes if world.g.nodes[n]['data'].type is NodeType.BANK]
        route_plan = self.find_greedy_way(world, [], bank_nodes, car.capacity, car.cash, car.position,
                                          world.current_time, car)
        return route_plan

    def find_greedy_way(self, world, route_plan, bank_nodes, capacity, current_cash, current_position, current_time, car):
        neighbors = [n for n in Graph.neighbors(world.g, current_position) if
                     n not in route_plan and n not in bank_nodes and world.g.nodes[n]['data'].cash != 0]
        bank_nodes = list(bank_nodes)
        route_plan = list(route_plan)
        best_choice_params = (None, None, None, None)
        for node in neighbors:
            edge = world.g.edges[(current_position, node)]
            target_data = world.g.nodes[node]['data']

            width = edge['data'].length * edge['data'].traffic[car.car_name]
            target_processing_end_time = current_time + width + target_data.time
            # Чтобы не упасть на функции расчета трафика
            if target_processing_end_time > world.end_time:
                continue
            # Плюс время возврата в банк из точки (Деньги не должны застрять на середине пути)
            closest_bank = self.get_closest_node(world, node, target_processing_end_time, bank_nodes, car)
            cumulative_cash = current_cash + target_data.cash
            if closest_bank[1] > world.end_time or cumulative_cash > capacity:
                continue
            current_price = target_data.cash / ((width + target_data.time) ** 2)
            if best_choice_params[0] is None or current_price > best_choice_params[1]:
                best_choice_params = (node, current_price, cumulative_cash, target_processing_end_time)
        if best_choice_params[0] is None:
            if current_position in bank_nodes:
                return route_plan  # Мы не можем уйти из банка (либо переполнены, либо не хватает времени)
            else:
                closest_bank = self.get_closest_node(world, current_position, current_time, bank_nodes, car)
                route_plan.append(closest_bank[0])
            return route_plan
        else:
            route_plan.append(best_choice_params[0])
            return self.find_greedy_way(world, route_plan, bank_nodes, capacity, best_choice_params[2],
                                        best_choice_params[0],
                                        best_choice_params[3], car)

    def get_closest_node(self, world, current, time, neighbors, car):
        closest_nodes = {}
        for node in neighbors:
            try:
                to_node_edge = world.g.edges[(current, node)]
                width = to_node_edge['data'].length * to_node_edge['data'].traffic[car.car_name]
                to_bank_road_time = time + width
                closest_nodes[node] = to_bank_road_time
            except:
                print('')
                raise BaseException
        closest_node = min(closest_nodes, key=closest_nodes.get)
        return closest_node, closest_nodes[closest_node]

    # Выполняем всякую мишуру.
    def try_serve(self, car, world):
        start_node_data = world.g.nodes[car.position]['data']
        # в целях упрощения предполагается что если машина отправилась к клиену то она его и обслуживает
        if start_node_data.type == NodeType.CLIENT and start_node_data.is_free and start_node_data.cash > 0:
            return AgentAction(type=AgentActionType.SERVE, car=car, target=car.position)
        # в целях упрощения предполагается что если машина в банке и у неё есть деньги то она разгружается
        if start_node_data.type == NodeType.BANK and car.cash > 0:
            return AgentAction(type=AgentActionType.SERVE, car=car, target=car.position)
