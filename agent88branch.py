from agent import AgentAction, AgentActionType
import time


class Path:
    def __init__(self, car, world):
        self.car = car
        self.world = world
        self.points = [car.position]
        self.cash = 0
        self.time = 0
        self.free = self.calc_free()

    def recalc(self):
        self.cash = self.calc_cash()
        self.time = self.calc_time()
        self.free = self.calc_free()

    def calc_free(self):
        busy_nodes = []
        for c in self.world.cars:
            for a in c.actions:
                busy_nodes.append(a)
        for p in self.points:
            busy_nodes.append(p)
        return [n[0] for n in self.world.g.nodes(data=True) if
                n[0] != 1 and n[1]['data'].cash != 0 and n[0] not in busy_nodes]

    def calc_cash(self):
        cash = self.car.cash
        cash_in_path = [n[1]['data'].cash for n in self.world.g.nodes(data=True) if
                        n[0] != 1 and n[0] in self.points]
        return cash + sum(cash_in_path)

    def calc_time(self):
        world = self.world
        path_time = 0
        for i in range(1, len(self.points)):
            start_time = world.current_time + path_time
            edge_data = world.g.edges[(self.points[i - 1], self.points[i])]['data']
            if self.points[i] != 1:
                edge_time = edge_data.length * edge_data.traffic[self.car.car_name]
            else:
                edge_time = edge_data.length * 2
            serv_time = world.g.nodes[self.points[i]]['data'].time
            path_time += edge_time + serv_time
        return path_time

    def is_time_ok(self):
        return self.world.current_time + self.time < self.world.end_time

    def is_cash_ok(self):
        return self.cash < self.car.capacity

    def __copy__(self):
        path = Path(self.car, self.world)
        path.points = []
        for i in self.points:
            path.points.append(i)
        path.free = []
        for i in self.free:
            path.free.append(i)
        return path

    def __str__(self):
        return "path {} takes {} -> {} gives ${} score: {}".format(self.points, self.time,
                                                                   self.time + self.world.current_time, self.cash,
                                                                   self.score())

    def score(self):
        if self.time == 0:
            return self.cash
        return self.cash ** 4 / self.time

    def compare(self, other):
        return self.score() - other.score()


class Agent88branch:
    def __init__(self, branches=2, time_limit=1, edge_limit=2):
        self.branches = branches
        self.time_limit = time_limit
        self.edge_limit = edge_limit
        pass

    def __str__(self):
        return "Agent88branch {} {} {}".format(self.branches, self.time_limit, self.edge_limit)

    def get_action(self, car, world):
        car.actions = []
        # car_node = world.g.nodes[car.position]['data']
        # if (car.position == 1 and car.cash > 0) or (
        #         car.position != 1 and car_node.cash > 0 and car_node.is_free):
        #     return AgentAction(type=AgentActionType.SERVE, car=car, target=car.position)

        # print("get_new_path:")
        path = Path(car, world)
        paths = []
        if car.position != 1:
            path.points.append(1)
            paths.append(path)

        if car.position == 1:
            for n in path.free:
                new_path = path.__copy__()
                new_path.points.append(n)
                new_path.points.append(1)
                new_path.recalc()
                if new_path.is_cash_ok() and new_path.is_time_ok():
                    paths.append(new_path)
                #     print("\t\t{} - good".format(new_path))
                # else:
                #     print("\t\t{} - bad".format(new_path))

        if len(paths) == 0:
            return AgentAction(type=AgentActionType.FINISH, car=car, target=car.position)

        branch_limit = self.branches
        if self.branches > len(paths):
            branch_limit = len(paths)
        time_limit = self.time_limit * 1000 + current_milli_time()
        paths.sort(key=lambda p: p.score(), reverse=True)
        self.update_paths(paths, car, world, branch_limit, time_limit)
        path = paths[0]

        car.actions = path.points
        print("i choose {}".format(path))
        return AgentAction(type=AgentActionType.MOVE, car=car, target=path.points[1])

    def update_paths(self, paths, car, world, branch_limit, time_limit):
        for i in range(0, branch_limit):
            path = paths[i]
            self.update_path(path, car, world, branch_limit, time_limit)
            if current_milli_time() > time_limit:
                return

    def update_path(self, path, car, world, branch_limit, time_limit):
        # print("try_update_path: {}".format(path))
        paths = []
        edge_limit = len(path.points)
        if len(path.points) > self.edge_limit:
            edge_limit = self.edge_limit
        for i in range(1, edge_limit):
            for free in path.free:
                new_path = path.__copy__()
                new_path.points.insert(i, free)
                new_path.recalc()
                if new_path.is_cash_ok() and new_path.is_time_ok():
                    paths.append(new_path)
                #     print("\t\t{} - good".format(new_path))
                # else:
                #     print("\t\t{} - bad".format(new_path))

        if len(paths) > 0:
            paths.sort(key=lambda p: p.score(), reverse=True)
            branch_limit = self.branches
            if self.branches > len(paths):
                branch_limit = len(paths)
            if paths[0].cash < car.capacity - 100000:
                self.update_paths(paths, car, world, branch_limit, time_limit)

            path.points = paths[0].points
            path.recalc()
            # print("\tupdate to {}".format(path))


def current_milli_time():
    return int(round(time.time() * 1000))
