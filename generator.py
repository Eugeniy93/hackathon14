import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import random
import math

from world import NodeData, NodeType, EdgeData


class MapGenerator:
    def __init__(self):
        pass

    def gen(self, size, seed):
        g = nx.Graph()
        random.seed(seed)

        for i in range(size):
            g.add_node(i,
                       data=NodeData(id=i, type=NodeType.CLIENT, time=random.randint(5, 15),
                                     cash=random.randint(20000, 400000)),
                       locked=False)

        g.nodes[0]['data'].type = NodeType.BANK
        g.nodes[0]['data'].cash = 0

        for i in range(size):
            for j in range(i, size):
                if i != j:
                    g.add_edge(i, j, data=EdgeData(traffic=1.0, length=random.randint(10, 100), id=(i, j)), weight=1)

        return g


class MapGeneratorXY:
    def __init__(self):
        pass

    def gen(self, size, seed):
        g = nx.Graph()
        random.seed(seed)

        for i in range(size):
            radius = random.random() * 90 + 10
            angle = random.randrange(0, 355)
            g.add_node(i,
                       data=NodeData(id=i, type=NodeType.CLIENT, time=random.randint(5, 15),
                                     cash=random.randint(20000, 400000)),
                       pos=(int(radius * math.cos(angle)), int(radius * math.sin(angle))),
                       locked=False)

        g.nodes[0]['data'].type = NodeType.BANK

        for i in range(size):
            for j in range(i, size):
                if i != j:
                    g.add_edge(i, j,
                               data=EdgeData(traffic=1.0, length=get_length(g.nodes[i]['pos'], g.nodes[j]['pos']), id=(i, j)),
                               weight=1)

        return g


def get_length(a, b):
    return math.sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)

class TrafficGenerator:
    def f(self, x):
        part = 1.0 / 4.0
        if part * 0 <= x < part * 1:
            return (0.25 - 0.5 * x) / 0.25
        if part * 1 <= x < part * 2:
            return (0.0625 + 0.25 * x) / 0.25
        if part * 2 <= x < part * 3:
            return (0.3125 - 0.25 * x) / 0.25
        if part * 3 <= x:
            return (0.5 * x - 0.25) / 0.25
        raise ValueError("function not defined for time {}".format(x))

    def draw(self):
        plt.figure()
        v = []
        for x in np.arange(0.0, 1.0, 0.01):
            v.append(self.f(x))
        plt.plot(v)
        plt.show()

    def get_traffic(self, edge, normal_time):
        return self.f(normal_time)

    def update_traffic(self, g, normal_time):
        for e in g.edges(data=True):
            e[2]['data'].traffic = self.f(normal_time)
