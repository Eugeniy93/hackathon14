from enum import Enum

import networkx as nx
import numpy as np
import math
from operator import attrgetter

SHORT_STR = True


class World:
    def __init__(self):
        self.end_time = 60 * 8
        self.start_time = 0
        self.current_time = 0
        self.cars = []
        self.g = None
        self.scheduler = Scheduler()
        self.traffic_generator = None
        self.waiting_traffic_refresh = False

    def normal_time(self):
        return self.current_time / (self.end_time - self.start_time)

    def normal_time_for(self, real_time):
        return real_time / (self.end_time - self.start_time)

    def is_cash_over(self):
        clients = [n[1]['data'] for n in self.g.nodes(data=True) if n[1]['data'].type == NodeType.CLIENT]
        remaining_cash = sum(c.cash for c in clients)
        cars_cash = sum(c.cash for c in self.cars)
        return remaining_cash == 0 and self.is_all_cars_free() and cars_cash == 0

    def get_score(self):
        return self.g.nodes[0]['data'].cash

    def is_time_over(self):
        return self.current_time >= self.end_time

    def is_valid_time(self, time):
        return time <= self.end_time

    def add_car(self, car):
        self.cars.append(car)

    def get_current_time(self):
        return self.current_time

    def is_all_cars_free(self):
        for car in self.cars:
            if not car.is_free:
                return False
        return True

    def get_free_cars(self):
        result = []
        for car in self.cars:
            if car.is_free:
                result.append(car)
        return result

    def get_moving_cars(self):
        result = []
        for car in self.cars:
            if not car.is_free and car.transition is not None:
                result.append(car)
        return result

    def get_fail_cars(self):
        result = []
        for car in self.cars:
            if not car.is_free or self.g.nodes[car.position]['data'].type != NodeType.BANK:
                result.append(car)
        return result

    def get_serving_cars(self):
        result = []
        for car in self.cars:
            if not car.is_free and car.position is not None:
                result.append(car)
        return result

    def schedule(self, time, action):
        # ceil_time = int(math.ceil(time)) # NOTE: нельзя бездумно округлять тут время... иначе расходится с расчетами
        self.scheduler.schedule(Action(time=time, action=action))

    def check_serve(self, car, target):
        target_data = self.g.nodes[target]['data']
        if car.position != target:
            raise ValueError("Mismatch car position {} and target {}".format(car.position, target))

        if target_data.type == NodeType.CLIENT:
            if not target_data.is_free:
                raise ValueError("Client {} already in process".format(target))

            new_cash = car.cash + target_data.cash
            if new_cash > car.capacity:
                raise ValueError("Cash overflow {}. Available capacity {}".format(new_cash, car.capacity))

    def serve(self, car, target):
        self.check_serve(car, target)
        target_data = self.g.nodes[target]['data']
        car.is_free = False
        target_data.is_free = False

        action_time = self.current_time + target_data.time

        def action():
            # print("car served: {}".format(car.position))
            car.is_free = True
            target_data.is_free = True
            if target_data.type == NodeType.BANK:
                target_data.cash += car.cash
                car.cash = 0
            elif target_data.type == NodeType.CLIENT:
                car.cash += target_data.cash
                target_data.cash = 0
            else:
                raise ValueError("Incorrect node type {}".format(target_data.type))

        # print("car serve...: {}".format(car.position))
        self.schedule(action_time, action)

    def check_move(self, car, target):
        if target not in self.g.neighbors(car.position):
            raise ValueError("Incorrect move from {} to {}. Available targets {}"
                             .format(car.position, target, list(self.g.neighbors(car.position))))

    def move(self, car, target):
        self.check_move(car, target)
        transition_data = self.g.get_edge_data(car.position, target)['data']
        target_node = self.get_node(target)
        if target_node.data.type != NodeType.BANK:
            self.g.nodes[target]['data'].is_used = True
        car.time_free = self.current_time + transition_data.width
        car.is_free = False
        car.transition = (car.position, target)
        car.position = None

        action_time = self.current_time + transition_data.width

        def action():
            # print("car moved: {}".format(car.transition))
            car.is_free = True
            car.position = target
            car.transition = None

        # print("car move...: {}".format(car.transition))
        self.schedule(action_time, action)

    def update_traffic(self):
        for e in self.g.edges(data=True):
            e[2]['data'].set_traffic(self.traffic_generator.get_traffic(e, self.normal_time()))

    def skip(self):
        if len(self.scheduler.actions) > 0:
            self.current_time = min(self.scheduler.actions, key=attrgetter('time')).time
        else:
            self.current_time = self.end_time
        # print("current_time: {}".format(self.current_time))

    def act(self):
        if not self.is_time_over():
            self.scheduler.run(self.current_time)

    def lock(self, target):
        target_data = self.g.nodes[target]['data']
        if target_data.type != NodeType.BANK:
            self.g.nodes[target]['locked'] = True

    def unlock(self, target):
        self.g.nodes[target]['locked'] = False

    def lock_all(self, targets):
        for t in targets:
            self.lock(t)

    def unlock_all(self, targets):
        for t in targets:
            self.g.nodes[t]['locked'] = False

    def is_locked(self, target):
        return self.g.nodes[target]['locked']

    def get_node(self, number):
        return NodeInfo(number, self.g.nodes[number]['data'])

    def get_edge(self, node_from, node_to):
        return EdgeInfo(node_from, node_to, self.g.edges[(node_from, node_to)]['data'])


class NodeType(Enum):
    BANK = 0
    CLIENT = 1

    def __str__(self):
        return self._name_


class NodeData:
    def __init__(self, type, time, cash, id):
        self.id = id
        self.type = type
        self.time = time
        self.cash = cash
        self.is_free = True
        self.is_used = False

    def __str__(self):
        if SHORT_STR:
            return "{}m - ${}\n{}".format(self.time, self.cash, self.is_free)
        else:
            return "{}\ntime:{}\ncash:{}\nfree:{}".format(self.type, self.time, self.cash, self.is_free)


class EdgeData:
    def __init__(self, traffic, length, id):
        self.id = id
        self.length = length
        self.traffic = {"sb0": traffic, "sb1": traffic, "sb2": traffic, "sb3": traffic, "sb4": traffic}

    def __str__(self):
        if SHORT_STR:
            return str(int(self.length * self.traffic['sb0']))
        else:
            return "traffic:{}\nlength:{}".format(round(self.traffic, 2), self.length)

    def __repr__(self):
        return self.__str__()


class Car:
    def __init__(self, capacity, node, car_name=None):
        self.car_name = car_name
        self.cash = 0
        self.capacity = capacity
        self.position = node
        self.transition = None
        self.is_free = True

        self.actions = []
        self.wait_server_response = False
        self.car_time = 0
        self.is_car_finished = False


class Action:
    def __init__(self, time, action):
        self.time = time
        self.action = action

    def run(self):
        self.action()


class Scheduler:
    def __init__(self):
        self.actions = []

    def schedule(self, action):
        self.actions.append(action)

    def get_actions_for_time(self, time):
        return [i for i in self.actions if i.time == time]

    def run(self, time):
        actions = [i for i in self.actions if i.time <= time]
        for a in actions:
            print(time)
            self.actions.remove(a)
            a.run()


class NodeInfo:
    def __init__(self, number, node_data):
        self.n = number
        self.data = node_data

    def __str__(self):
        return "number: {} data: {}".format(self.n, self.data)


class EdgeInfo:
    def __init__(self, orig, dest, edge_data):
        self.n1 = orig
        self.n2 = dest
        self.data = edge_data

    def __str__(self):
        return "node from: {} node to: {} data: {}".format(self.n1, self.n2, self.data)


class Empty:
    pass
