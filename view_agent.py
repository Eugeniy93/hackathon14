import networkx as nx
import time

from enum import Enum

from agent import AgentAction, AgentActionType
from world import NodeType


class ViewAgent:
    def __init__(self):
        pass

    def __str__(self):
        return "GodAgent"

    def calc_rewards(self, context, depth, current_time, current_cash, start):
        targets = [target for target in context.world.g.neighbors(start) if not self.is_locked(context.world, target)]
        targets.sort(reverse=True,
                     key=lambda target: self.move_reward_function(context.world, current_time, start, target))
        result = [(target, self.calc_reward_lock_wrapper(context, depth,
                                                         current_time, current_cash,
                                                         start, target))
                  for target in targets]
        return result

    def get_action(self, car, world):
        context = CalcContext(world, car.capacity, world.current_time, time.time())
        rewards = self.calc_rewards(context, 0, world.current_time, car.cash, car.position)
        best_move, best_reward = max(rewards, key=lambda i: i[1])
        print("best reward: {}".format(best_reward))
        print("best route: {}".format(context.max_route))

        if self.concurrent_enabled and context.max_route:
            world.lock_all(context.max_route)
            self.routes[car] = context.max_route.copy()

        if best_reward > 0:
            world.lock(best_move)
            return AgentAction(type=AgentActionType.MOVE, car=car, target=best_move)
        else:
            return AgentAction(type=AgentActionType.FINISH, car=car, target=car.position)


class CalcContext:
    def __init__(self, world, car_capacity, world_time_start, real_time_start):
        self.current_route = []
        self.max_reward = 0
        self.max_route = None
        self.world = world
        self.car_capacity = car_capacity
        self.world_time_start = world_time_start
        self.real_time_start = real_time_start

    def is_locked(self, target):
        return self.world.g.nodes[target]['locked']

    def lock(self, target):
        target_data = self.world.g.nodes[target]['data']
        if target_data.type != NodeType.BANK:
            self.world.g.nodes[target]['locked'] = True

    def unlock(self, target):
        self.world.g.nodes[target]['locked'] = False

    def route_push(self, target):
        self.current_route.append(target)

    def route_pop(self, target):
        self.current_route.pop()

    def route_save(self, reward):
        if reward > self.max_reward:
            print("{} > {}".format(reward, self.max_reward))
            self.max_reward = reward
            self.max_route = self.current_route.copy()

    def node_data(self, target):
        return self.world.g.nodes[target]['data']

    def node(self, target):
        return self.world.g.nodes[target]

    def edge_data(self, start, target):
        return self.edge(start, target)['data']

    def edge(self, start, target):
        return self.world.g.edges[(start, target)]

    def unlocked_neighbors(self, start):
        return [target for target in self.world.g.neighbors(start) if not self.is_locked(target)]

    def update_max_reward_route(self, new_max_reward):
        # print("{} > {}".format(new_max_reward, self.max_reward))
        self.max_reward = new_max_reward
        self.max_route = self.current_route.copy()
