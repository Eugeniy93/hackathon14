import random

from networkx import Graph
from world import NodeType, EdgeInfo, NodeInfo, Empty
from agent import AgentActionType, AgentAction


class SimpleGreedyAgent:

    def get_action(self, car, world):
        # serve = self.serve(car, world)
        # if serve is not None:
        #     return serve

        move = self.move(car, world)
        if move is not None:
            return move

        return AgentAction(type=AgentActionType.FINISH, car=car, target=car.position)

    def get_closest_node(self, world, car, bank):
        current_node_number = car.position
        neighbour_list = list(Graph.neighbors(world.g, car.position))
        best = Empty()
        best.node = -1
        best.utility = -1
        has_some_path = False
        for neighbour_node_number in neighbour_list:
            neighbour_node = world.get_node(neighbour_node_number)
            if neighbour_node.data.is_used or neighbour_node.data.type == NodeType.BANK or neighbour_node.data.cash == 0:
                continue
            neighbour_edge = world.get_edge(current_node_number, neighbour_node_number)
            if not self.is_enough_time(world, neighbour_node, neighbour_edge, bank):
                continue
            elif not self.is_enough_capacity(car, neighbour_node):
                continue
            else:
                # if car.cash < 750000:  # or (car.cash > 750000 and random.randint(0, 100) < 5):
                has_some_path = True
                current_to_neighbour_time = neighbour_edge.data.length * neighbour_edge.data.traffic + neighbour_node.data.time
                # neighbour_to_bank_time = world.get_edge(neighbour_node_number, bank.n).data.get_width(world)
                money = neighbour_node.data.cash
                utility = money / current_to_neighbour_time
                if utility > best.utility:
                    best.node = neighbour_node
                    best.utility = utility  # Сделать функцию выбора точки поумнее

        if not has_some_path:
            if car.position == bank.n:
                return None
            else:
                return bank
        elif best is not None:
            return best.node

    def is_enough_time(self, world, neighbour, neighbour_edge, bank):
        current_time = world.get_current_time()
        neighbour_process_time = neighbour_edge.data.length * neighbour_edge.data.traffic + neighbour.data.time
        neighbour_bank_edge = world.get_edge(neighbour.n, bank.n)
        bank_process_time = neighbour_bank_edge.data.length * neighbour_bank_edge.data.traffic + bank.data.time
        return world.is_valid_time(current_time + neighbour_process_time + bank_process_time)

    def is_enough_capacity(self, car, neighbour):
        return car.capacity >= car.cash + neighbour.data.cash

    def serve(self, car, world):
        node = world.get_node(car.position)
        node_data = node.data
        # в целях упрощения предполагается что если машина отправилась к клиену то она его и обслуживает
        if node_data.type == NodeType.CLIENT and node_data.cash > 0:
            return AgentAction(type=AgentActionType.SERVE, car=car, target=node.n)
        # в целях упрощения предполагается что если машина в банке и у неё есть деньги то она разгружается
        if node_data.type == NodeType.BANK and car.cash > 0:
            return AgentAction(type=AgentActionType.SERVE, car=car, target=node.n)

    def move(self, car, world):
        bank = world.get_node(1)
        next_node = self.get_closest_node(world, car, bank)
        if next_node is not None:
            return AgentAction(type=AgentActionType.MOVE, car=car, target=next_node.n)
        else:
            return None
