import json
import os
import random
import math


def write(filename, value):
    if os.path.isfile(filename):
        os.remove(filename)
    with open(filename, 'a') as json_file:
        json.dump(value, json_file)


def read(filename):
    with open(filename) as json_file:
        return json.load(json_file)


def anal_points():
    points = read("points.json")
    min = 999999999
    max = 0
    sum = 0
    count = 0
    for p in list(points['points']):
        sum += p['money']
        count += 1
        if min > p['money']:
            min = p['money']
        if max < p['money']:
            max = p['money']
    print("min: {} max: {} avr: {}".format(min, max, sum / count))
    print("i think: 3000 + rand(97000) \n")
# min: 3328 max: 95431 avr: 48433.560975609755
# 3000 + rand(97000)

def anal_routes():
    routes = read("routes.json")
    min = 999999999
    max = 0
    sum = 0
    count = 0
    for p in list(routes['routes']):
        sum += p['time']
        count += 1
        if min > p['time']:
            min = p['time']
        if max < p['time']:
            max = p['time']
    print("min: {} max: {} avr: {}".format(min, max, sum / count))
    print("i think: 1 + rand(30) \n")
# min: 1 max: 31 avr: 14.215853658536586
# 1 + rand(30)?


def anal_traffic():
    min = 999999999
    max = 0
    sum = 0
    count = 0

    for i in range(0, 4):
        routes = read("traffic_{}.json".format(i))
        for p in list(routes['traffic']):
            jam = float(p['jam'])
            sum += jam
            count += 1
            if min > jam:
                min = jam
            if max < jam:
                max = jam
    print("min: {} max: {} avr: {}".format(min, max, sum / count))
    print("i think: 1 + rand(1) \n")
# min: 1.0 max: 2.0 avr: 1.4985731707317043
# 1 + rand(1)


def anal_traffic_changes_all():
    data = {}
    for i in range(0, 4):
        routes = read("traffic_{}.json".format(i))
        for p in list(routes['traffic']):
            a = p['a']
            b = p['b']
            jam = float(p['jam'])

            key = a * 1000 + b
            if key not in data:
                data[key] = {'a': a, 'b': b}
            data[key][i] = jam

    min = 999999999
    max = 0
    sum = 0
    count = 0
    for k in data:
        t = data[k]
        diff0 = abs(t[0] - t[1])
        diff1 = abs(t[1] - t[2])
        diff2 = abs(t[2] - t[3])
        sum += diff0 + diff1 + diff2
        count += 3
        if min > diff0:
            min = diff0
        if max < diff0:
            max = diff0
        if min > diff1:
            min = diff1
        if max < diff1:
            max = diff1
        if min > diff2:
            min = diff2
        if max < diff2:
            max = diff2
    print("min: {} max: {} avr: {}".format(min, max, sum / count))
    print("i think: wht \n")

def anal_traffic_changes_01():
    data = {}
    for i in range(0, 2):
        routes = read("traffic_{}.json".format(i))
        for p in list(routes['traffic']):
            a = p['a']
            b = p['b']
            jam = float(p['jam'])

            key = a * 1000 + b
            if key not in data:
                data[key] = {'a': a, 'b': b}
            data[key][i] = jam

    min = 999999999
    max = 0
    sum = 0
    count = 0
    for k in data:
        t = data[k]
        diff0 = abs(t[0] - t[1])
        sum += diff0
        count += 1
        if min > diff0:
            min = diff0
        if max < diff0:
            max = diff0
    print("min: {} max: {} avr: {}".format(min, max, sum / count))
    print("i think: wht \n")


def anal_traffic_changes_12():
    data = {}
    for i in range(1, 3):
        routes = read("traffic_{}.json".format(i))
        for p in list(routes['traffic']):
            a = p['a']
            b = p['b']
            jam = float(p['jam'])

            key = a * 1000 + b
            if key not in data:
                data[key] = {'a': a, 'b': b}
            data[key][i] = jam

    min = 999999999
    max = 0
    sum = 0
    count = 0
    for k in data:
        t = data[k]
        diff1 = abs(t[1] - t[2])
        sum += diff1
        count += 1
        if min > diff1:
            min = diff1
        if max < diff1:
            max = diff1
    print("min: {} max: {} avr: {}".format(min, max, sum / count))
    print("i think: wht \n")


def anal_traffic_changes_23():
    data = {}
    for i in range(2, 4):
        routes = read("traffic_{}.json".format(i))
        for p in list(routes['traffic']):
            a = p['a']
            b = p['b']
            jam = float(p['jam'])

            key = a * 1000 + b
            if key not in data:
                data[key] = {'a': a, 'b': b}
            data[key][i] = jam

    min = 999999999
    max = 0
    sum = 0
    count = 0
    for k in data:
        t = data[k]
        diff2 = abs(t[2] - t[3])
        sum += diff2
        count += 1
        if min > diff2:
            min = diff2
        if max < diff2:
            max = diff2
    print("min: {} max: {} avr: {}".format(min, max, sum / count))
    print("i think: wht \n")


def print_table():
    routes = read("traffic_0.json")
    for p in list(routes['traffic']):
        a = p['a']
        b = p['b']
        jam = float(p['jam'])
        key = a * 1000 + b
        print("{}\t{}".format(key,jam))


if __name__ == "__main__":
    print('anal_points')
    anal_points()
    print('anal_routes')
    anal_routes()
    print('anal_traffic')
    anal_traffic()
    print('anal_traffic_changes_01')
    anal_traffic_changes_01()
    print('anal_traffic_changes_12')
    anal_traffic_changes_12()
    print('anal_traffic_changes_23')
    anal_traffic_changes_23()
    print('anal_traffic_changes_all')
    anal_traffic_changes_all()
    print_table()
    pass