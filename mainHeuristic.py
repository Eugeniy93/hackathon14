from agent import AgentActionType
from agent import AgentActionType
from generator import MapGenerator, TrafficGenerator, MapGeneratorXY
from simple_greedy_agent import SimpleGreedyAgent
from util import generate
from world import World, Car
from model import Solution, Path, Loop
from heuristic_agent import HeuristicAgent
import util
import copy

if __name__ == "__main__":

    world = generate()

    # agent = GodAgent()
    # 50XY = 2973690 50=4118906 250XY*5 = float division by zero 250*5=20506434

    # agent = GreedyAgent()
    # 50XY = 2707166 50=2948406 250XY*5 = 10378960 (fail car: 3) 250*5=14316627

    agent = GreedyAgent()
    # 50XY = 2971852 50=2928996 250XY*5 = 11691758 250*5=15803976

    solution = Solution()
    while not world.is_time_over() and not world.is_cash_over():
        world.update_traffic()

        for car in world.get_free_cars():
            agent_action = agent.get_action(car, world)
            if agent_action.type == AgentActionType.MOVE:
                world.move(agent_action.car, agent_action.target.n)
            elif agent_action.type == AgentActionType.SERVE:
                world.serve(agent_action.car, agent_action.target.n)
            elif agent_action.type == AgentActionType.FINISH:
                pass
            else:
                raise ValueError("Incorrect agent action type: {}".format(agent_action.type))

            if agent_action.type == AgentActionType.MOVE:
                solution.add_point(car, agent_action.target.n)

        world.skip()
        world.act()

    util.print_result(world, solution)

    # best_score = 0
    #
    # for i in range(10000):
    #     world = generate()
    #
    #     try:
    #         heuristic = HeuristicAgent()
    #         solution = heuristic.permutate(world, copy.deepcopy(solution))
    #         util.emulate(world, solution)
    #         score = world.get_score()
    #         if score > best_score:
    #             best_score = score
    #
    #         util.print_result(world, solution)
    #     except:
    #         continue
