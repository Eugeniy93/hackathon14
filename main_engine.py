import threading
import time

from agent import AgentActionType
from agent88branch import Agent88branch
from engine import Engine
from god_agent import GodAgent
from greedy_agent import GreedyAgent
from simple_greedy_agent import SimpleGreedyAgent

def start_engine_thread(engine):
    engine.ws.run_forever()


if __name__ == "__main__":
    lock = threading.Lock()
    engine = Engine(lock)
    engine_thread = threading.Thread(target=start_engine_thread, args=(engine,), daemon=True)
    engine_thread.start()
    time.sleep(5)
    engine.register_team()
    # Ждем когда придут данные
    time.sleep(5)

    # agent = GodAgent()
    # 50XY = 2973690 50=4118906 250XY*5 = float division by zero 250*5=20506434

    agent = GreedyAgent(lock)
    # 50XY = 2707166 50=2948406 250XY*5 = 10378960 (fail car: 3) 250*5=14316627

    # agent = SimpleGreedyAgent()

    # agent = Agent88branch()
    # Деньги 1480899
    world = engine.world

    while True:
        if world.g is not None and not world.waiting_traffic_refresh:
            for car in world.cars:
                if not car.wait_server_response and car.car_time < world.end_time and not car.is_car_finished:
                    world.current_time = car.car_time
                    agent_action = agent.get_action(car, world)
                    if agent_action.type == AgentActionType.MOVE:
                        engine.moveCar(car, agent_action.target)
                        if agent_action.target != 1:
                            car.cash += world.g.nodes[agent_action.target]['data'].cash
                            world.g.nodes[agent_action.target]['data'].cash = 0
                        if agent_action.target == 1:
                            world.g.nodes[agent_action.target]['data'].cash += car.cash
                            car.cash = 0
                    if agent_action.type == AgentActionType.SERVE:
                        agent_action = agent.get_action(car, world)
                        engine.moveCar(car, agent_action.target)
                        if agent_action.type == AgentActionType.FINISH:
                            car.is_car_finished = True
                    if agent_action.type == AgentActionType.FINISH:
                        car.is_car_finished = True
        isAllCarsMoved = True
        for car in world.cars:
            if not car.is_car_finished:
                isAllCarsMoved = False
        if isAllCarsMoved:
            break

    print("Деньги {}".format(engine.teamsum))
    print("Деньги локально {}".format(world.g.nodes[1]['data'].cash))
