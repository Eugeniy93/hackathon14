class Solution:
    def __init__(self):
        self.paths = {}

    def add_point(self, car, point):
        path = self.paths.get(car)
        if path is None:
            path = Path()
            self.paths[car] = path
        path.add_point(point)

    def get_point(self, car, number):
        return self.paths.get(car).get_point(number)

    def __str__(self):
        result = ""
        for car, path in self.paths.items():
            result += "car: {}, path: {} \n".format(car, path)
        return result

    def __repr__(self):
        return self.__str__()


class Path:
    def __init__(self):
        self.loops = []

    def add_point(self, point):
        if point == 0:
            self.loops[-1].closed = True
        else:
            last = None
            if len(self.loops) == 0:
                last = Loop()
                self.loops.append(last)
            else:
                last = self.loops[-1]
            if last.closed:
                last = Loop()
                self.loops.append(last)
            last.add_point(point)

    def get_point(self, number):
        for loop in self.loops:
            if number > len(loop.points):
                number = number - len(loop.points) - 1
            elif number == len(loop.points):
                return None
            else:
                return loop.points[number]

    def __str__(self):
        result = ""
        for loop in self.loops:
            result += "{} \t".format(loop)
        return result

    def __repr__(self):
        return self.__str__()


class Loop:
    def __init__(self):
        self.money = 0
        self.start = 0
        self.end = 0
        self.points = []
        self.closed = False

    def add_point(self, point):
        # self.money += point.data.cash
        self.points.append(point)

    def __str__(self):
        result = ""
        for point in self.points:
            result += " {} ".format(point)

        return "[" + result + "]"

    def __repr__(self):
        return self.__str__()
